@extends('layouts.main')

@section('content')
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one" >
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>ORGANISATIONAL ACHIEVEMENTS</h1>
								<ul>
									<li><a href="{{ route('index') }}">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{ route('Organization.achievements') }}">organisation Achievements</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
			<br>
			<!-- Blog Details ____________________________ -->
			<section class="">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12 margin-top">
              <div class="panel panel-default" style="padding: 15px;">

							<div class="Blog-Details-Wrapper">

                <h4 style="color: #7F1F21;">Where Tamana was the <strong>FIRST</strong></h4>
								<p>&emsp;&emsp;&#9673;&nbsp;One of the first Special Schools in India, "Tamana Special School" was started by Tamana to provide special education to the mentally challenged and multiple disability at a time when special education services were non existent in India.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana was the first special needs institution to conceptualise and start a Residential Training Programme for adults with separate units where they can get the feel of a home (1998). This programme referred as the Night Stay Programme was made a compulsory part of our curriculum.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana highlighted the concept of formal school certification for special needs students. For two years the course coordinators worked on curriculum planning and then put into operation the first Special Accredited Centre of NIOS in 1999.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana was the first to think of a recreation club where special adults can interact with their peers and get a chance to build up their confidence and self esteem The Fun club started in 2000 runs on the weekend and is open to all special needs individual who may or may not be students of Tamana.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana was the first institution to recognize autism as a disability distinct from others and the first to start programmes for Autistic Spectrum Disorders.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana publishes the only bilingual newsletter ""autism news"" for disseminating information about the latest research and interventions in field of autism. It has readership all over India.</br>
                &emsp;&emsp;&#9673;&nbsp;In 2004 Tamana became the first NGO to start a non governmental residential facility for the mentally challenged and autistic in Delhi.</br>
                &emsp;&emsp;&#9673;&nbsp;In 2004, Tamana was selected by WHO to develop a training program of international standard to train master trainers working with intellectually disabled in the South East Asian Region countries.</br>
                &emsp;&emsp;&#9673;&nbsp;In 2005, Tamana was granted Consultative status with United Nation's Economic and Social Council.</br>
                &emsp;&emsp;&#9673;&nbsp;In July 2005, Tamana set up two sheltered workshops – Bakery at Nai Disha Vocational Centre and Laundry at Tamana Special school.</br>
                &emsp;&emsp;&#9673;&nbsp;In 2006, Tamana is the first NGO to introduce Reverse Integration through its Tamana Kindergarten Project.</br>
                &emsp;&emsp;&#9673;&nbsp;Tamana has been in the forefront for spreading awareness about autism in India. It has organised three international conferences(2003, 2006, 2010) and three international workshops(2003, 2008, 2010) to bring the latest research and interventions on autism to India.</p>
								<center><div class="blog-details-img-one"><img src="{{ asset('images/orgach.jpg')}}" style="width: 50%;" alt="image"></div></center><!-- /.blog-details-img-one -->
                <h4 style="color: #7F1F21;">Tamana’s Autism Center ranked <strong>India’s #1</strong> Special School by the Education World </h4>
                <p>Tamana’s Autism Center was recently ranked India’s #1 Special School by the Education World in its annual India School Rankings. Acclaimed for providing state-of-the-art educational facilities and for its technological research in Autism, this recognition is a matter of great pride for the Tamana family and its well-wishers</p>
								<div class="blog-details-bg-img">
									<div class="opacity">
										<p>First Donation, Understand Behavior</p>
									</div> <!-- /.opacity -->
								</div> <!-- /.blog-details-bg-img -->
</div>
</div>
</div>
</div>
</div>
</section>
@endsection

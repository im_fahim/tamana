@extends('layouts.main')

@section('content')
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Recognition</h1>
								<ul>
									<li><a href="{{ route('index') }}">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('recognition.achievements')}}">Recognition</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
			<br>

			<!-- Blog Details ____________________________ -->
			<section class="">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12 margin-top">
              <div class="panel panel-default" style="padding: 15px;">

							<div class="Blog-Details-Wrapper">
								<h4 style="color: #7F1F21;">Tamana Ngo was facilitated with the Rashtriya Swayamsiddh Samman</h4>
		              <p>Tamana Ngo was facilitated with the Rashtriya Swayamsiddh Samman for our contribution in the field of Innovation and Technology by JSPL foundation at Kamani auditorium, New Delhi, on 14th January 2016. The award was given by Shri. Naveen Jindal, Chairman, Jindal Steel & Power Ltd, Mr. C. Raj Kumar, Vice Chancellor, O. P. Jindal Global University.</p>
	                         <table style="width:100%">
 	                           <tbody>
                               <tr>
                                 <td><img src="{{asset('images/recg1.jpg')}}" border="0" style="width:304px;"></td>
                                 <td><img src="{{asset('images/recg2.jpg')}}" border="0" style="width:304px;"></td>
                                 <td><img src="{{asset('images/recg3.jpg')}}" border="0" style="width:304px;"> </td>
                              </tr>
                            </tbody>
                          </table>
                          <br>
								<p>Tamana was awarded the “ICT led Innovation getting a Catalytic Grant” by NASSCOM in its Leadership Forum 2015 on the 11th of February. The award was received by Mr. Blessin Varkey led Research team- for developing Kinect based educational applications for individuals with autism and special needs.</p>
                <center><img src="{{asset('images/recg4.jpg')}}" border="0" style="width:304px;"></center>
                <br>
                <p>Tamana was awarded the “ICT led Innovation getting a Catalytic Grant” by NASSCOM in its Leadership Forum 2015 on the 11th of February. The award was received by Mr. Blessin Varkey led Research team- for developing Kinect based educational applications for individuals with autism and special needs.</p>
                <center><img src="{{asset('images/recg5.jpg')}}" border="0" style="width:304px;"></center>
                <br>
                <p>Tamana was awarded the National Award for Best Institution for Child Welfare by Department of Women and Child Development, Government of India. The award was presented by Hon'ble Minister for Women and Child Development Mrs Krishna Tirath on 23rd Jan in her office at Shastri Bhawan. An excited Tamana Chona received the award on be half of the institution. Tamana receives recognition from Govt. Of India for its services.</p>
                <center><table style="width:70%">
                  <tbody>
                    <tr>
                      <td><img src="{{asset('images/recg6.jpg')}}" border="0" style="width:304px;"></td>
                      <td><img src="{{asset('images/recg7.jpg')}}" border="0" style="width:304px;"></td>
                   </tr>
                 </tbody>
               </table></center>
               <br>
               <p>The first Mother Teresa Award (Nov. 1997) was presented to Tamana Special School for its dedicated services to the Mentally Impaired.</p>
               <p>Dr. (Mrs.) Shyama Chona, President of Tamana, received the National Award for the Welfare Of People With Disabilities for the year 1997. It was an appreciation of years of struggle in the field of disability at a personal and institutional level for her.</p>
               <p>NGO in Special Consultative Status with United Nation's Economic and Social Council, 2005.</p>
               <p>National Award for Best Institution for Child Welfare 2006 from the Ministry of Women and Child Development , Govt of India.</p>
							 <p>Award for the Most Innovative Project Implementation in 2007 presented at the round table organised on Global Partnerships In Poverty Eradication And Health Care organised as part of annual ministerial review innovation fair (Asia)- An initiative of NGO DESA-United Nations Economic and Social Council & Mumbai Educational trust.</p>






</div>
</div>
</div>
</div>
</div>
</section>
@endsection

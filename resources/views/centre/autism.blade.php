@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one" style="background: url({{asset('images/centres/1.JPG')}}) no-repeat center center;background-size: cover;background-attachment: fixed;">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Autism Centre</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('Autism.centre')}}">Autism Centre</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="col-md-6">
							<p>Tamana was the first institution to recognize autism as a disability distinct from others and to start programs for autistic spectrum disorder in 1985. The Autism Centre-School of Hope is India’s first rehabilitate and research center for autistic individual providing holistic services under one roof – a special school, sensory integration clinic, early intervention center, diagnostic center, research cell and an outreach cell.<br><br>
							His Excellency Dr. A.P.J. Abdul Kalam the president of India inaugurated Autism Centre-Tamana, a full-fledged school dedicated to autistic, on 19 August 2003. State of the art intervention is offered at the School Of Hope for children from ages 2 years onwards with severe needs including autistic spectrum disorders and multiple disabilities. This initiative provides productive education and therapy to hundreds who do not fit existing special education programmes. </p>
						</div>
						<div class="col-md-6">

							<center><img src="{{asset('images/centres/aut/hope 1.JPG')}}" style="width: 100%"></center>
						</div>
						<div class="col-md-12">
						<p>
							<br>
						The highlights of the program offered in this school are :- <br><br>
            &emsp;&emsp;&#9673;&nbsp;A life centred approach to curriculum planning to meet the needs of child and family.<br>
            &emsp;&emsp;&#9673;&nbsp;An eclectic combination of different approaches to develop a holistic approach to meet individual needs. Some of the approaches on which intervention for the autistic is based are TEACCH, ABA (with small group of students), and the therapeutic use of drama, music, dance, horse riding, and yoga. The behavior management strategies are designed according to individual needs.  Positive reinforcement techniques are commonly used for teaching.<br>
            &emsp;&emsp;&#9673;&nbsp;Sensory Integration therapy is an important part of intervention programme.<br>
            &emsp;&emsp;&#9673;&nbsp;For the high functioning integrated group academics is a vital curriculum area Montessori Method and material is used in classrooms with adaptations according to the individual child.<br>
            &emsp;&emsp;&#9673;&nbsp;Computer with latest software is regularly used for overall development of the students.<br>
            &emsp;&emsp;&#9673;&nbsp;Music and dance therapy<br>
            &emsp;&emsp;&#9673;&nbsp;Yoga and Sports<br>
            &emsp;&emsp;&#9673;&nbsp;Activities of daily living and managing his/ her environment are taught in real life siuations-kitchen, bathroom and dressing room. The students learn to make beds, arrange cloths in cupboards, personal grooming, washing clothes, cooking.<br>
            &emsp;&emsp;&#9673;&nbsp;Vocational Training for youth above 16 yrs of age-Vocational skills taught are  jewelry design, greeting cards making, gardening, <br><br>
          </p>
          </div>
					<div class="col-md-12">
						<center><img src="{{asset('images/centres/aut/hope 2.JPG')}}" style="width: 50%"></center>
					</div>
				</div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Address:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>CPWD Complex<br>
                Near Chinmaya Vidyalaya<br>
                Vasant Vihar, New Delhi.<br>
                Tel: 26143843/26153474<br>
                Principal - Ms Usha Varma<br>
                Meeting time with the Principal -10am--12pm (Mon-Fri)<br>
                Email - <a href="info@tamana.org">info@tamana.org</a><p>
              </div>
              <div class="col-md-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.45246877306!2d77.15601201508088!3d28.556172782448048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1dba8b5b30f5%3A0x412ebeae9593273f!2sChinmaya+Vidyalaya!5e0!3m2!1sen!2sin!4v1517810189672" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>

            </div>
          </div>
        </div>

      </div>




@endsection

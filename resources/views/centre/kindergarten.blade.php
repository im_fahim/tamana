@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Tamana Kindergarten</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('Kindergarten.centre')}}">Kindergarten</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
						<h6 style="color:#7F1F21">Early Childhood care and Education Programme for Children from Economically Disadvantaged groups.</h6><br>
            <p> Padma Bhushan, Dr Shayama Chona is the inspiration and driving force behind the preschool. As a committed educationist and a social activist her belief is that education alone could help these underprivileged children to build dreams and become useful citizens of new and resurgent India, where all will have a roof over their heads, an open mind to think and everyone will be equal in all terms.</P>
							<br><h6 style="color:#7F1F21"> Project Concept</h6><br>
						<p> The first 6 years of life are critical for development in humans as the rate of development is the most rapid in this period; consequently this is the time when environmental deprivation or enrichment makes the maximum impact. Since development is proceeding at a very fast rate, unfavourable experiences such as lack of adequate food, health care, nurturance or stimulation, unhealthy living conditions hinder development to a considerable extent. In the same way favourable conditions foster development. The effect is long lasting since the foundation for development is laid in early years. <br>
							 The significance of early childhood years is being increasingly acknowledged. This recognition assumes greater importance with reference to underprivileged children. The limitations imposed by poverty and its socio cultural manifestations lead to multiple deprivations during critical early years of life preventing these children from realizing their full potential. Families from low socio economic background are unable to provide a healthy environment, adequate nutrition and stimulating opportunities for their children. As large no. of women from this strata have to spend most of their time earning livelihood, it becomes necessary in thee circumstances to have early childhood care and education programmes as an organized intervention to compensate the deprivation faced by children of weaker sections.<br>
							  Tamana Kindergarten provides free early childhood care and education for children living in the slums. Recognising the holistic nature of child development, viz, nutrition, health and social, mental physical and moral, emotional development, this programme of early childhood care and education gives priority to these areas. The programme is be child oriented and will focus around play, i.e. non formal education. The local community is fully involved in this programme.<br>
								 The programme is currently running in the premises of three schools of Tamana which remain unutilized for several hours in the afternoon. The project employs teachers trained in early child hood care. The working hours of existing administrative staff and class IV employees of Tamana have been extended to perform similar duties for afternoon school. This cost effective model will be easily replicable for the spread of   non formal preschool education, accessible to as many children as possible.<br></p>
								 <br><h6 style="color:#7F1F21"> Reverse integration</h6><br>
								 <p> Tamana Kindergarten which started as a platform for providing preschool education to the disadvantaged has now taken a new vision and new meaning- it is now being projected as a landmark model of Reverse Integration. Since it is difficult to integrate our special children in the normal school. We have started mainstreaming the preschoolers of our morning session with our special children of similar ages. Initially the children are brought together for co curricular activities. Students of Tamana Kindergarten have been performing with Tamana students in all Tamana Events. This Kindergarten school will eventually lay the foundation for setting up a full fledged integrated set up at Tamana, since mainstreaming is the new mantra. We will keep on increasing classes every year as the students’ progress to the next academic level. <br></p>
								 <br><h6 style="color:#7F1F21"> Objectives: </h6><br>
								<p>
						&emsp;&emsp;&#9673;&nbsp;To provide an integrated early childhood care and education programme for the under privileged children.<br>
            &emsp;&emsp;&#9673;&nbsp;To offer nutritional, health and educational services to these children. <br>
            &emsp;&emsp;&#9673;&nbsp;To used play way methods, activity based approach interspersed with formal learning.<br>
            &emsp;&emsp;&#9673;&nbsp;To identify the hidden talents of the underprivileged children and to foster their talents.<br>
            &emsp;&emsp;&#9673;&nbsp;To give the under privileged children a better quality of thought and life.<br>
            &emsp;&emsp;&#9673;&nbsp;To create awareness among the slum dwellers about education, hygiene, environment, health and self dependence<br>
            &emsp;&emsp;&#9673;&nbsp;To convert people into useful human resource.<br>
            &emsp;&emsp;&#9673;&nbsp;To empower the slum children and their m others and bring them into the main stream of the society.<br>
            &emsp;&emsp;&#9673;&nbsp;To evolve a learning system for marginalized children with family as its basic units.<br>
						&emsp;&emsp;&#9673;&nbsp;To integrate these preschooler with our special needs students<br><br>
          </p>
					<br><h6 style="color:#7F1F21"> Target Population: </h6><br>
					<p> Children (2.5- 6 years) with denied opportunities, children ready for schooling, working children, children who never attended school and street children with special emphasis is on girls. </p><br>
					<p> <strong>Curriculum:</strong> Non formal, preschool education for class nursery and prep NCERT curriculum for class 1.
						 <br><strong>Medium of Instructions:</strong> Hindi and bi-lingual
						  <br><strong>Entry point:</strong> Nursery, Prep and I. On the basis of chronological readiness for School.</p><br>
							<h6 style="color:#7F1F21"> Timings: </h6><br>
							<p>&emsp;&emsp;&#9673;&nbsp;9.30 am - 12.30 pm (School of Hope)<br>
							&emsp;&emsp;&#9673;&nbsp;2 pm - 5 pm (Tamana Special School) <br></p><br>
							<h6 style="color:#7F1F21"> Programme: </h6><br>
							<p>
								&emsp;&emsp;&#9673;&nbsp;Time-table designed with due weight age to extra curricular activities and sports.<br>
								&emsp;&emsp;&#9673;&nbsp; Regular Medical camps will be organized for health check ups of children and free immunization.<br>
								&emsp;&emsp;&#9673;&nbsp; Music, Dance, Art & Craft, Aerobics, Games.<br>
								&emsp;&emsp;&#9673;&nbsp; Nutritional Mid day meal will be provided every day.<br>
								&emsp;&emsp;&#9673;&nbsp; Class Competitions will be held regularly such as Fancy dress, drawing competitions, story telling, recitations, puppet show, quiz etc.<br>
								&emsp;&emsp;&#9673;&nbsp; Educational trips and picnics will be organized.9.30 am - 12.30 pm (School of Hope)<br>
								&emsp;&emsp;&#9673;&nbsp; Annual function, national and social festivals will be celebrated.<br>
								&emsp;&emsp;&#9673;&nbsp; Educational films will be screened.<br>
								&emsp;&emsp;&#9673;&nbsp; Health and cleanliness awareness programs will be organized.<br>
								&emsp;&emsp;&#9673;&nbsp; Counselling session for parents on health, hygiene, importance of education, nutrition.<br>



          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Contact:</div>
          <div class="panel-body">
						<p><strong>Head Mistress:</strong> Ms Meenakshi,<br>
						<strong>Contact No:</strong> +919971673008</p>
          </div>
        </div>

      </div>




@endsection

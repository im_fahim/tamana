@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one" style="background: url({{asset('images/centres/skl/banner.JPG')}}) no-repeat center center;background-size: cover;background-attachment: fixed;">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Skill Development Centre</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('SkillDevelopment.centre')}}">Skill Development Centre</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <p>
            Nai Disha, as the same name signifies, is a new direction taken by Tamana. Nai Disha was conceived with the realization that the young adults at Tamana have to be equipped with skills to adapt appropriately to the needs of adulthood and thereby function as an independent whole-physically and emotionally. The program aims at creating an infrastructure, which ensures a smooth graduation from school to the outside world, for the young adult and the family. It also aims to ensure training and placements of the young adults in various vocations and organizations. This can either be done independently in the residential center or by associating with various sheltered workshops and training institutions. The vocational training program aims at the following:- <br><br>
            &emsp;&emsp;&#9673;&nbsp;To develop relevant vocational skills and competence among the intellectually and multiply disabled youth to improve their quality of life.<br>
            &emsp;&emsp;&#9673;&nbsp;To serve intellectually and multiply disabled youth, emphasizing school to career and other career readiness programs in the partnership with the parents, disabled friendly industries and corporate houses.<br>
            &emsp;&emsp;&#9673;&nbsp;Build awareness about career and employment options for intellectually and multiply disabled individual who face significant barriers to employment; and provide education, training and workplace experiences so they may actively pursue these options.<br>
            &emsp;&emsp;&#9673;&nbsp;Provide education in the fundamentals of business and economics, the importance of savings, the basics of personal financial management and related consumer issues.<br><br>
            Students are assessed for ability and interest in learning skills for life vocations at Skill Development Centre-Tamana. The areas of training are varied and dynamic and are sometimes introduced to suit the needs of particular students.<br>
						 Besides the vocational training the curriculum also includes formal education through  National Open School  from class 3 to 10, sports and yoga, therapeutic interventions (like speech and occupational therapy), extracurricular activities like music, dance and art and activities of daily living, computer training.<br></p>
						 <h5 style="color:#7F1F21">SPECIFIC OPTIONS INCLUDE THE FOLLOWING:</h5><br>
						 <h6>Textiles Printing</h6><br>
						  <p>Over the years this unit has grown into a sheltered workshop catering to orders round the year. Young adults here are trained on every aspect of screen printing, block printing tie and dye -starting from selecting the kind of materiel used for mixing of colors, printing, dying and curing of all the items made. A wide range of products are designed in this unit- silk / chiffon block printed scarves, kurtas, skirts and coasters; screen printed hand towels, T- shirts, table cloths, tea cozies; block printed and tie and dye dupattas.<br><br>
								<div class="container">
								<div class="col-md-6">
									<center><img src="{{asset('images/centres/skl/tp1.JPG')}}" style="width: 100%"></center>
								</div>
								<div class="col-md-6">
									<center><img src="{{asset('images/centres/skl/tp2.JPG')}}" style="width: 100%"></center>
								</div>
							</div>
							<br>

							</p><h6> Office skill management </h6><br>
							 <p>Wherein depending on the competencies, a student is trained in office skills like typing, filing, photocopying, simple banking operations, stock-taking and answering phone calls.<br>
							  Apart from this a computer centre at the school functions to equip the young adult with secretarial and accounting training for possible job placement besides enhancing their thinking skills and reasoning ability. This centre models the path for awareness, exploration, preparation, placement and follow up.<br><br>
							</p><h6>Paper Bag And Envelop making</h6><br>
								  <p>The aim of starting this unit was that it should filtrate of the market by selling and displaying in the shops, apart from the catering to the individual orders.<br>
									 A core team of students is selected for this unit, keeping in mind their interest and competence. Each crease and fold is done by the young adult of our school with / without assistance of a teacher. Every effect is made to check the esthetics value and quality of the product. At present we are catering to individual orders, stores and organizations.<br><br>
								 </p><h6>Paper recycling </h6><br>
										 <p>The world is going ecofriendly and we wanted to the cause, thus started the concept of recycling unit. Here students are involved in tearing the newspaper and other rough paper are used at school. The student’s there by learns to make hand made papers. which are used for selling, making files, folders and other products.<br><br>
									 </p><h6>Clay Modeling</h6><br>
											 <p>Pottery has become a very popular in the present day world as a hobby and as a subject because of its therapeutic nature and educative value. Here the students learn to kneed the clay, mould it and give the unique designs. Gradually they are picking up the dynamics of the product and its usefulness at home and in the market.<br><br>
										 </p>
										 <div class="col-md-6 col-xs-12">
										 <h6>Baking</h6><br>
											  <p>Baking (bread, cakes biscuits) is a simple cooking activity and after the process is broken into simple steps (task analysis) like identifying ingredients, weighing the ingredients, mixing, transferring to moulds, baking, decorating, packing and making the bills. Technological advancements have made the process simpler as now state of the art baking equipment are available which are easily operable by a single button and simplest of instructions.  Additionally baked products have a good market in Indian urban areas and are fast moving products. The sales of products help us in raising funds for our organization and providing stipend to students who train in it.  Thus, leading to greater self-sufficiency at student and institutional level. At present there are 12 students working in our bakery unit. A special educator and a baker train these students.<br><br></p></div>
													<div class="col-md-6 col-xs-12"><center><img src="{{asset('images/centres/skl/bake.JPG')}}" style="width: 75%"></center></div><br>

											<div class="col-md-12"><h6>File Making </h6><br>
												  <p>A new vocation has been introduced at Nai Disha Vocational Center -manufacturing Cobra office files. This unit was set up keeping in mind our philosophy of introducing vocations which can easily be learned by our students and the end product has a demand in the market. The fact that the machines are manually operated makes the learning accident free. The file making process has been broken into simple sequences. A core group of 9 students have been selected to make the files. The process of file making involves cutting, creasing, putting rivets, moulding the edges and printing. We hope to cater to the offices, nearby markets and schools. As this unit is simple in its functionality, we hope our young adults may open up a sheltered unit later in life.<br><br>
													 Other skills that are taught are - <br>

            &emsp;&emsp;&#9673;&nbsp;Home management unit - basic home management skills of cooking, baking, serving and computer skills training<br>
            &emsp;&emsp;&#9673;&nbsp;Candle making<br>
            &emsp;&emsp;&#9673;&nbsp;Stationary Production - Making greeting cards, envelops, writing pads, file folders <br>
            &emsp;&emsp;&#9673;&nbsp;Gardening<br>
						&emsp;&emsp;&#9673;&nbsp;Beauty culture<br>
						&emsp;&emsp;&#9673;&nbsp;Tailoring<br>
          </p></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Address:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>Tamana Naidisha<br>
                  C-10/8 Vasant Vihar, New Delhi<br>
                 Principal - Ms. Anita Pandey <br>
                 Meeting time with the Principal -10am--12pm (Mon-Fri)<br>
                Email - <a href="info@tamana.org">info@tamana.org</a><p>
              </div>
              <div class="col-md-8">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.0605617417546!2d77.15505935051465!3d28.567944082357094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1da8e7f18f73%3A0x9c51b1691e43b17d!2sNai+Disha!5e0!3m2!1sen!2sin!4v1518588775793" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
            </div>
          </div>
        </div>

      </div>




@endsection

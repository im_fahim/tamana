@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one" style="background: url({{asset('images/centres/2.JPG')}}) no-repeat center center;background-size: cover;background-attachment: fixed;">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Special Education Centre</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('SpecialEducation.centre')}}">Special Education Centre</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <p>
            The first branch of Tamana started in 1984 in tent. The school was shifted to present premises on 12th Feb. 1992 after inauguration by late lady Diana. Tamana special school caters to the individual needs of 115 children coming from all sections of the society,  age ranging from 4-17 years. At Tamana special school, the program focuses on over all development of child depending on needs as well as potential. Functional academics are imparted with parallel intervention with allied therapies depending on the needs of the child. The regular school curriculum is a vital reference points for students who have potential to be included in a regular classroom or join the open school program.<br><br>
						 After detailed assessment by special educator, psychologist, occupational therapist and speech therapist students are divided into groups according to their chronological age, mental age and functional level. Each group consists of 6 students with one special educator, one assistant teacher or volunteers. The individualized program is made for each child with the help of multy-disciplinary team. Parents are equally involved in this process. While setting the goals the family needs are given priority. After every three month each child’s progress is assessed and on basis of performance short-term goals are delineated for the next quarter. In this process teachers and parents are equal partners- both meet and discuss the observations at school and home. On regular interval parents are called for class observation.<br><br>
						  Apart from regular services that form the core of any effective program for the special needs population, what sets Tamana apart, is its emphasis on other alternative strategies and therapies such as :-<br><br>
							 Computer Training: A comprehensive computer training programme aims at making the child proficient in basic applications like - MS word, power point, MS excel, Ms outlook express, internet etc. Most children with consistent access to a computer over the period are eventually able to power up the machine, start up a pre-loaded application either from the hard disk or a CD-ROM, quit an application, operate the printer, access the Internet from the desktop, and even send an email, download pictures or images etc. The academic curriculum has been shifted on computer for the students who have difficulty in writing. Adaptive facilities are provided to the students according to the individual needs. Computer training is provided to all students.<br><br>
							  Community service: An interactive programme of children from regular schools and Tamana students runs with the aim of establishing meaningful friendships, self esteem, confidence and social skill development.<br><br>
								 Speech therapy programme: In addition to the regular speech therapy programme, ABA Programme and behavioural principles are used to get the best results.<br><br>
								  Yoga therapy: This helps the adolescents in enhanced co-ordination, increased attention span and reducing hyperactivity.<br><br>
									 Creative multi-sensory activities: Are facilitated through rhythmic songs and dance in the junior classes. Students are also taking training in skating, swimming and horse riding.<br><br>
									  Networking: With other schools from India and abroad. In this project student write mails to each other on regular basis; send reply to the queries of partner school; exchange projects to create awareness services and programs in their countries.<br><br>
										 Integrated arts, which include drama, dance and music as a therapy and as means of skills acquisition.<br><br>
										  Rajiv Gandhi Outreach Centre<br><br>
											 Tamana not only caters to Delhi but also to the other parts of the country. Parents come from different parts of the country for assessment, counseling & for home program. The team of specialist plans the program. The family comes from their respective destinations and stay in Delhi for a week. They are trained how to carry on the home program. They can continue the program at home and come back for discussion about the progress and difficulties or they can contact the authorities via mail or telephone. Similarly an outreach program runs for the children who cannot be settled in regular school program. Intensive training session by a special educator and therapist is taken in which parent or the caretakers are also being trained. Once the child is ready for the school then he/she admitted in the school.<br><br>
											  Vocational Training<br><br>
												 Students with moderate to severe category of mental retardation are introduced to pre- vocational training once the students reach the age of 14. Having limited mental capacities these students cannot be expected to continue in a purely educational program till adulthood like their normal counterparts. After they have learnt functional academics they have to be trained in skills, which will lead to economic independence and rehabilitation. The pre- vocational unit gives vocational training to enhance their capabilities to make them useful member of the society. In this centre our emphasis is training them in any vocational skill according to the ability of our students for developing them into self- sufficient and self-reliant young adults. These trained skills can be carried out from home under the supervision of any family member or attendant. Under this training area skills covered are-<br><br>
            &emsp;&emsp;&#9673;&nbsp;Paper bag making. Students are taught to decorate the bags with block printing and sticking & pasting of designs on the bags.<br>
            &emsp;&emsp;&#9673;&nbsp;Stain glass painted products.<br>
            &emsp;&emsp;&#9673;&nbsp;Making collage, decorative stationery like gift tags, wrapping papers, hand made cards, small pads.<br>
            &emsp;&emsp;&#9673;&nbsp;Spiral binding of diaries and pads.<br>
            &emsp;&emsp;&#9673;&nbsp;Painting Diyas, Paper Mache products.<br>
            &emsp;&emsp;&#9673;&nbsp;Gardening<br>
            &emsp;&emsp;&#9673;&nbsp;Pen assembly<br>
            &emsp;&emsp;&#9673;&nbsp;Jewelry making.<br>
            &emsp;&emsp;&#9673;&nbsp;Beauty culture.<br>
						&emsp;&emsp;&#9673;&nbsp;Embroidery.<br><br>
          </p>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Address:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>Tamana Special School<br>
                 D-6 street Vasant Vihar, New Delhi 110057.<br>
                Principal - Ms. Rashmi Wahi<br>
                telephones - 011 26151572<br>
                 Meeting time with the Principal -10am--12pm (Mon-Fri)<br>
                Email - <a href="info@tamana.org">info@tamana.org</a><p>
              </div>
              <div class="col-md-8">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14017.903808041607!2d77.1608084!3d28.5554673!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc30d8707d62e5675!2sTamanna+Special+School!5e0!3m2!1sen!2sin!4v1517811620829" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>

            </div>
          </div>
        </div>

      </div>




@endsection

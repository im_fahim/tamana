<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themazine.com/html/chcharity/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 31 Jan 2018 04:59:38 GMT -->
<head>
		<meta charset="UTF-8">
		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">


		<title> Tamana || A non profit NGO for multiply challenged and autistic individuals </title>


		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="{{ asset('images/logo/favicon-1.png')}}">

		<!-- Main style sheet -->
		<link rel="stylesheet" href="{{ asset('css/style.css')}}">
		<!-- responsive style sheet -->
		<link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
		<link rel="stylesheet" href="{{ asset('css/parsley.css')}}">


		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="main-page-wrapper">



<!-- Header _________________________________ -->
<section class="header-section">
  <div class="top-header top-header-v-two">
    <div class="container">
      <div class="clear-fix">
        <ul class="float-left top-header-left">
          <li><a href="{{route('pages.volunteer')}}"><i class="fa fa-hand-paper-o" aria-hidden="true"></i> Volunteer</a></li>
					<li><a href="{{route('pages.vacancy')}}"><i class="fa fa-briefcase" aria-hidden="true"></i> Vacancies</a></li>
          <li><a href="{{route('pages.contact')}}"><i class="fa fa-mobile" aria-hidden="true"></i> Contact</a></li>
					<li><a href="{{route('panel.index')}}"><i class="fa fa-drivers-license" aria-hidden="true"></i> Panel</a></li>

        </ul> <!-- /.top-header-left -->
        <ul class="float-right top-header-right">
          <li class="Our-Help"><a href="#">Support Autism</a></li>
          <li><a href="https://www.facebook.com/tamana.ngo"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="https://twitter.com/tamanango"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="https://www.youtube.com/channel/UCZY4IlUaKXwTZ7BHIWwNQQA"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
        </ul> <!-- /.top-header-right -->
      </div> <!-- /.clear-fix -->
    </div> <!-- /.container -->
  </div> <!-- /.top-header -->
  <div class="middle-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="them-logo"><a href="/"><img src="{{ asset('images/logo/logo.png')}}" alt="logo"></a></div><!-- /.them-logo -->
			  </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->
  </div> <!-- /.middle-header -->

@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Contact Us</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('pages.contact')}}">Contact Us</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="well well-sm">
          <div class="row">
            <div class="col-md-4">
              <h5 style="color:#7F1F21">TAMANA</h5><br>
              <p> Head office/admission office C-10/8, Vasant Vihar<br>
                 New Delhi - 57<br>
                 <strong>Phone:</strong> +91-11-26148269/26151587<br>
                  <strong>Telefax:</strong> +91-11-26148269<br>
                  <strong>Email:</strong><a href="info@tamana.org">info@tamana.org</a><br>
            </div>
            <div class="col-md-8">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.0592031094475!2d77.15526141464251!3d28.567984882443316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1da4ffffffff%3A0x5b4f10d71e56a21d!2sTamana+Delhi.!5e0!3m2!1sen!2sin!4v1518761814251" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>          </div>
      </div>
      </div>
      <hr>
      <div class="well well-sm">
        <div class="row">
          <div class="col-md-8">

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14017.07043010491!2d77.14847637494283!3d28.56172581913021!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1dade09ce82d%3A0xc30d8707d62e5675!2sTamanna+Special+School!5e0!3m2!1sen!2sin!4v1518762228166" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="col-md-4">
            <h5 style="color:#7F1F21">TAMANA SPECIAL SCHOOL</h5><br>
            <p> D-6, Street, Vasant Vihar<br>
               New Delhi - 57<br>
               <strong>Phone:</strong> +91-11-26151572<br>
                <strong>Telefax:</strong> +91-11-26151572<br>
                <strong>Email:</strong><a href="tamanaspecialschool@tamana.org">tamanaspecialschool@tamana.org</a><br></div>
    </div>
</div>
<hr>
<div class="well well-sm">
  <div class="row">
    <div class="col-md-4">
      <h5 style="color:#7F1F21">NAI DISHA</h5><br>
      <p> C-10/8, Vasant Vihar<br>
           New Delhi - 57<br>
           <strong>Phone:</strong> +91-11-26151572<br>
            <strong>Telefax:</strong> +91-11-26151572<br>
          <strong>Email:</strong><a href="naidisha@tamana.org">naidisha@tamana.org</a><br>
    </div>
    <div class="col-md-8">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.0605617388924!2d77.15506471464255!3d28.567944082443304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1da8e7f18f73%3A0x9c51b1691e43b17d!2sNai+Disha!5e0!3m2!1sen!2sin!4v1518762561202" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>
</div>
<hr>
<div class="well well-sm">
  <div class="row">
    <div class="col-md-8">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14017.070504912997!2d77.14847637220146!3d28.561725257390794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1daddc8edca9%3A0x8dfa09521b1abe83!2sSchool+of+Hope+Tamana+Association!5e0!3m2!1sen!2sin!4v1518762370622" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>
    <div class="col-md-4">
      <h5 style="color:#7F1F21">AUTISM CENTRE - SCHOOL OF HOPE</h5><br>
      <p> C.P.W.D. Complex, Near Chinmaya Vidyalaya<br>
          Vasant Vihar,New Delhi<br>
         <strong>Phone:</strong> +91-11-26153474/26143853<br>
          <strong>Email:</strong><a href="schoolofhope@tamana.org">schoolofhope@tamana.org</a><br>
</div>
</div>
</div><hr>


</div>


@endsection

@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Staff Vacancies</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('pages.vacancy')}}">Vacancy</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center>Tamana has following vacancies </center></div>
          <div class="panel-body">
            <br><h5 style="color:#7F1F21"><u>Tamana TTC Faculty Requirement DSE-MR/HI/ASD</u></h5>
            <br><h6> <strong>1. Name of the Post: - Lecturer in Special Education (Hearing Impairment)</strong></h6>
            <br><p><strong>Essential Qualification: -</strong><br>
              &emsp;&emsp;&#9673;&nbsp; M.Ed.SE (HI)/B.ED.SE(HI) with M.A in Psychology/ Social Science / Humanities & Science or Teaching Subjects with 2/3 years of teaching experience at Diploma Programmes. <br>
              &emsp;&emsp;&#9673;&nbsp; The Candidates must have valid registration certificate with RCI.<br>
              <strong>Desirable:-</strong>Basic Computer Knowledge</p><br>
              <h6> <strong> 2. Name of the Post: - Lecturer in Clinical Psychology/Psychology.</strong></h6>
              <br><p><strong>Essential Qualification: -</strong><br>
                &emsp;&emsp;&#9673;&nbsp;M.Phil in Clinical Psychology/ M.Phil in Rehabilitation Psychology/ PGDRP with 2/3 years teaching Experience in Psychology in any Special Education Institute. <br>
                &emsp;&emsp;&#9673;&nbsp; The Candidates must have valid registration certificate with RCI.<br>
                <strong>Desirable:-</strong>Basic Computer Knowledge</p><br><br>
                <h6> <strong> Situation Vacant for for 2 Special Educators for hostel at the Skill Development Centre for Young Adults</strong></h6>
                <br><p> <strong>Qualification:</strong> (DSE -MR./ ASD) B. ED in Spl. Education/PGDMR. M.Ed in Special Education (for high functioning groups). Regular B.Ed (with experience of working with special children) Candidates with Proficiency in English will be given preference. </p><br>
                <h6><strong>Situation Vacant for Occupational Therapist ( 1 in No) for Tamana Autism Centre </strong></h6>
                <br><p> <strong>Qualification:</strong> BOT/MOT in Occupational Therapy from a recognised institute/ university. The applicant should have 2-3 years of experience working with children with autism and having knowledge of sensory integration. Proficiency in written and spoken English is a must. </p><br>
                <h6><strong>Situation Vacant for Music Teacher </strong></h6>
        </div>
      </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center>Address</center></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>The Director (Schools)<br>
                  Tamana<br>
                  C-10/8, Vasant Vihar<br>
                   New Delhi - 110057<br>
                    Phone: 26148269/26151587<br>
                Email - <a href="jobs@tamana.ngo">jobs@tamana.ngo</a><br>
              </div>
              <div class="col-md-8">
<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d14017.07043010489!2d77.15027612494283!3d28.56172581913021!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s+The+Director+(Schools)+Tamana+C-10%2F8%2C+Vasant+Vihar!5e0!3m2!1sen!2sin!4v1518761177973" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>							</div>
            </div>
          </div>
        </div>

      </div>




@endsection

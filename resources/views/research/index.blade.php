@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one" style="background: url({{asset('images/research/banner.JPG')}}) no-repeat center center;background-size: cover;background-attachment: fixed;">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Tamana Research</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('research.index')}}">Research</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center><strong>Tamana’s Research, Innovation & Technology Lab</strong></center></div>
          <div class="panel-body">
            <p>Over the last decade, Tamana has encouraged many research scholars from various universities to conduct their Masters and Doctoral dissertation at Tamana. </p><br><br>
            <h6 style="color:#7F1F21">TOBY PLAYPAD</h6><br>
            <p>Tamana’s first foray into technology-based-research was in 2013 with a unique collaboration with Deakin University Australia on TOBY Playpad - a unique, therapist –and-parent designed Early Intervention application for children with Autism. Tamana worked with Deakin on the development of a culturally-adaptive hindi (prototype) version of the early intervention application. Deakin for its work with Tamana, won the Victorian International Education Award for Excellence in International Education – Research Engagement in 2013. </p><br><br>
            <h6 style="color:#7F1F21">IBM Research</h6><br>
            <p>IBM Research: In December 2013, Tamana collaborated with IBM Research & TAUCHI to develop two Kinect based applications targeting retail purchase activities for students with learning disabilities, social development for students with Autism Spectrum Disorder and to provide design guidelines towards whole body gesture-based embodied learning systems. The research was accepted by the prestigious ACM Journal under “Promoting Joint Attention with Computer Supported Collaboration in Children with Autism” on the 17th of August 2015. </p><br><br>
            <p><cite>C.1] Sharma, S., Srivastava, S., Achary, K., Varkey, B., Heimonen, T., Hakulinen, J.S., Turunen, M. and Rajput, N., 2016, February. Promoting Joint Attention with Computer Supported Collaboration in Children with Autism. In Proceedings of the 19th ACM Conference on Computer-Supported Cooperative Work & Social Computing (pp. 1558-1569). ACM. </cite></p><br><br>
            <h6 style="color:#7F1F21">NIOS Research</h6><br>
            <p> Tamana is collaborated with NIOS Research to Study the effect of 2D and 3D Virtual Instructional Modules, for the Intellectually Challenged secondary students enrolled in the National Institute of Open Schooling, as a project of NIOS <strong>(National Institute of Open School)</strong> Research. </p><br><br>
            <h6 style="color:#7F1F21"> MoSJE Research </h6><br>
            <p> In January 2016, Tamana was granted a research grant in studying “The role of virtual training and skill development programme (in modern vocational programmes - Bio fertilizers & Solar Lantern) for the intellectually challenged and autistic individuals “ </p><br><br>
            <h6 style="color:#7F1F21"> University of Tampere, Finland </h6><br>
            <p> Tamana is also working towards “Design, develop and deploy interactive learning application for teaching life-skills for children with special needs within the classroom environment” with researchers from the Computer Science Dept, TAUCHI, University of Tampere, Finland. </p>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center><strong>Technology Projects</strong></center></div>
          <div class="panel-body">
            <div class="col-md-6">
						<h6 style="color:#7F1F21"> HOPE (Kinect based application for children with special needs)</h6><br>
            <p> Based on the success of the research with IBM, Tamana developed a prototype application on the Kinect aimed at enhancing motor coordination, joint attention and cognitive skills of children with autism. The prototype was awarded by NASSCOM at its Social Innovation Forum 2015 for its ICT based social innovation. The goal of the application is also to invite developers in India to work on the Kinect for enhancing cognition and assisting in improving the Fine/Gross motor skills of Individuals with special needs. </p>
						</div>
						<div class="col-md-6">
							<center><img src="{{asset('images/research/HOPE.JPG')}}" style="width: 50%"></center>
						</div>
						<br><br>


            <h6 style="color:#7F1F21">  Google for Education</h6><br>
            <p> Tamana recently became the first special school to be enrolled in Google for Education in India. Google Apps for Education is a suite of software programs designed for structuring the school structure, academics and curriculum. </p><br><br>
            <h6 style="color:#7F1F21"> IT Skills Training Programme</h6><br>
            <p> Microsoft recently sponsored 20 laptops to Tamana to initiate its IT skill development program. The IT skill development program seeks to train potential special students on the basics of computing and programming aiming at IT based employability. </p><br><br>
            <h6 style="color:#7F1F21"> ECHO India</h6><br>
            <p> Tamana partnered with ECHO India in late 2015 to disseminate knowledge and experience of experts to other likeminded organizations and individuals by means of conducting online sessions across various parents and professionals across India. </p>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center><strong>Awards</strong></center></div>
          <div class="panel-body">
            <p><strong> 2015</strong> – NASSCOM Social Innovation Award for HOPE (Kinect based application for children with special needs) </p><br>
            <p><strong> 2015</strong> – Winner of the eNGO Challenge, South Asia for suing online presence and digital tools to work for inclusion of differently abled. </p><br>
            <p><strong>2016</strong> – Rashtriya Swayamsiddh Shri Award by JSPL Foundation </p><br>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white"><center><strong>Contact</strong></center></div>
          <div class="panel-body">
            <p>We would like to partner with Universities, Researchers and Organizations to create ICT based accessible technologies for the special needs individuals as a step towards accelerating the inclusion process. You can contact us at e-mail id: <a href="research@tamana.org">research@tamana.org</a></p><br>
            <center>
              <h6>Person to Contact</h6></center>
            <div class="row">
              <div class="col-md-8">
                <p>Mr. Blessin Varkey,<br>
                  Associate Director -Innovation & Technology <br>
                 Email-:<a href="blessinvarkey@tamana.ngo">blessinvarkey@tamana.ngo</a></p> <br>
              </div>
              <div class="col-md-4">
              <p>  Dr. Krishnaveni Acharya,<br>
                Sr. Researcher, Autism <br>
                Email - <a href="krishnaveni@tamana.ngo">krishnaveni@tamana.ngo </a></p>
              </div>
            </div>
          </div>
        </div>

      </div>




@endsection

@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Boy's Hostel</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('services.apna-fun-club')}}">NIOS</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <p>
              Situated on the premises of Tamana's Vocational Training Center - Nai Disha, is a hostel that provides a unique program for adults and adolescents with disabilities. It aims to equip the mentally impaired and autistic person with all the skills that make for a life of least dependence on others and inculcate a feeling of self-sufficiency and self worth. The students are trained to become fully independent in all spheres of life. The residential center works towards the fulfillment of the following objectives:
            </p>
          </br>
            <p>
              &emsp;&emsp;&#9673;&nbsp;To provide a temporary residential facility to a family facing any crisis.<br>
              &emsp;&emsp;&#9673;&nbsp;To provide an independence training program.<br>
              &emsp;&emsp;&#9673;&nbsp;To provide for a constructive recreation time and enhance social interaction with others.<br>
            </p>
            <br>
            <p>
Admission is open to males in the age group 10 to 20 yrs, with mild to moderate levels of developmental disability. The program is designed to provide services for 20 individuals at a given time. The facility has well furnished dormitories. These are airy, sun-lit rooms. They are designed to facilitate programs to teach the students independent living skills – cooking, managing their own living area and laundry. Equipment like washing machines, irons, ovens etc. are available to accord a well-rounded training. Toilets and bathrooms have been planned especially to suit the needs of the special needs clientele. Facilities like television, VCD and music system are provided. Appropriate reading material, including newspapers and magazines are available in the library. The building also houses a well equipped gym with a treadmill, exercise cycles, steppers and multi-gym. The building has space and facilities for indoor and outdoor games. The short stay facility for a few days (Respite care) is available for Tamana students whereby parents can leave their children in the Tamana hostel for a few days whenever they wish to go out of station. This center also functions as an extended day care- wherein children of working parents stay till evening.            </p>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Address:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>Tamana Naidisha<br>
                  C-10/8 Vasant Vihar, New Delhi<br>
                 Principal - Ms. Anita Pandey <br>
                 Meeting time with the Principal -10am--12pm (Mon-Fri)<br>
                Email - <a href="info@tamana.org">info@tamana.org</a><p>
              </div>
              <div class="col-md-8">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.0605617417546!2d77.15505935051465!3d28.567944082357094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1da8e7f18f73%3A0x9c51b1691e43b17d!2sNai+Disha!5e0!3m2!1sen!2sin!4v1518588775793" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
            </div>
          </div>
        </div>

      </div>




@endsection

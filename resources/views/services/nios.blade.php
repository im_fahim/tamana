@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>NIOS</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('services.apna-fun-club')}}">NIOS</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <h4 style="color: #7F1F21;">NATIONAL INSTITUTE OF OPEN SCHOOLING FOR SPECIAL INDIVIDUALS</h4>

            <p>
              Touched by such questions of a special child Apana Fun Club was started in 1999 to provide the special needs young adults as their right to leisure activities & socialization. Special Educators Prithvi Perepa and Harpreet Singh worked hard to have this project take wings in the year .
            </p>
          </br>
            <p>
              Apana Fun Club has been functioning successfully for the past eleven years. The club is aimed at providing a platform for social interaction for intellectually challenged adults and adolescent. The recreational club runs on Saturdays between 9.30 am to 1.30 pm at Nai Disha. The club provides facilities for sports, excursions, trekking, library, T.V. room, creative arts, dance & movements, computer skills and yoga. The idea is to giving the special needs individual continuos opportunities to develop physical fitness, demonstrate courage, experience joy and participate in the sharing of gifts, skills and friendship with other special needs adult and also the sense of belonging to a group.
            </p>
            <br>
            <p>
              The club is open for special needs individuals who are above 15 years of age. All adolescents and young adults with disabilities are welcome to register as members.
            </p>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Address:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p>Tamana Naidisha<br>
                  C-10/8 Vasant Vihar, New Delhi<br>
                 Principal - Ms. Anita Pandey <br>
                 Meeting time with the Principal -10am--12pm (Mon-Fri)<br>
                Email - <a href="info@tamana.org">info@tamana.org</a><p>
              </div>
              <div class="col-md-8">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.0605617417546!2d77.15505935051465!3d28.567944082357094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1da8e7f18f73%3A0x9c51b1691e43b17d!2sNai+Disha!5e0!3m2!1sen!2sin!4v1518588775793" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
            </div>
          </div>
        </div>

      </div>




@endsection

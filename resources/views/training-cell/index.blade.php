@extends('layouts.main')

@section('content')
<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>TEACHERS TRAINING CEL</h1>
								<ul>
									<li><a href="/">Home</a></li>
									<li><span>-</span></li>
									<li><a href="{{route('training.index')}}">Teachers Training Cell</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Button</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>
      <br>
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-body">
            <p>
              August 12, 1994 saw the inauguration of the <strong>TEACHERS TRAINING CELL</strong> with the introduction of a year-long certificate course in Special Education (for teaching the mentally impaired and Autistic children). Tamana (School of Hope) is the recognized teacher training institute recognized by RCI. The courses offered are: <br>
              &emsp;&emsp;&#9673;&nbsp;Diploma in Education Special Education (Autism Spectrum Disorders) D. Ed. SE (ASD)<br>
              &emsp;&emsp;&#9673;&nbsp;Diploma in Education Special Education (Mental Retardation) D.Ed.SE (MR) <br>
              &emsp;&emsp;&#9673;&nbsp;Diploma in Education Special Education (Hearing Impairment) D.Ed.SE (HI) <br>
              &emsp;&emsp;&#9673;&nbsp;Diploma in Vocational Rehabilitation (Mental Retardation) DVR (MR) (One Year Course)<br></p><br><br>
              <p>These Programmes are being offered by the National institutes, NIEPMD (National Institute for Empowerment of Persons with Multiple Disability) ; NIEPID (National Institute for the Empowerment of Persons with Intellectual Disabilities) and AYJNISHD (Ali Yavar Jung National Institute of Speech, and Hearing Disabilities (Divyangyan) </p>
              <p>&emsp;&emsp;&#9673;&nbsp;Special educator in special school<br>
              &emsp;&emsp;&#9673;&nbsp;Special educator in inclusive set –up/regular school<br>
              &emsp;&emsp;&#9673;&nbsp;Special educator in a clinical / hospital set-up<br>
              &emsp;&emsp;&#9673;&nbsp;Special educator in Govt.schools under the SSA/DSSSB programme <br>
              &emsp;&emsp;&#9673;&nbsp; As a regular teacher in schools (2 yrs Diploma Programmes in special education in MR and ASD are equivalent to JBT/DIET). <br>
              &emsp;&emsp;&#9673;&nbsp;The trainee teacher will be fully trained to work in the home based training or open own center.<br><br></p>
              <h6 style="color:#7F1F21"><strong>Eligibility for the course</strong></h6><br>
              <p><strong>The minimum qualification for admission is 10+2 / Higher Secondary Examination/equivalent with minimum 50% for GEN/OBC category, 45% for SC/ST, 45 % for PH.</strong></p><br>
              <p style="color:#715F36"><strong>Note:-The medium of instruction for all the programme will be English/Hindi.</strong><br></p>
              <p>&emsp;&emsp;&#9673;&nbsp;We also conduct short-term courses and workshops designed individually as per the need of the group and certified by Tamana <br>
                &emsp;&emsp;&#9673;&nbsp; (We had conducted a six months pilot project of WHO for teachers of SAARC countries, three months training programme for teachers from LEH.) <br>
                &emsp;&emsp;&#9673;&nbsp;We have also been conducting International conferences and workshops on regular basis. <br><br></p>
                <p>These Special Education programmes aim to develop professionals for special and inclusive education within a broad perspective of education. The programme intends to educate and train the aspirants to become agents of change as teaching- professionals by imbibing the required knowledge, skills and positive attitude. The education and training will enable them to seek out and hone the talents in the children with disability. It will also catalyze the realization that special education is all about child centric good quality education.) Special preference is given to those from rural backgrounds, from economically weaker sections of society and parents of the children with disability for the courses.</p>
                <br><h6 style="color:#7F1F21">&#9673; ADMISSION OPEN IN TEACHER TRAINING CELL</h6><br>
                <p>
                   Admission open for RCI recognised full time Diploma in Education -special education programmes in the following programmes<br>
                   &emsp;&emsp;&nbsp;<strong>1.</strong>Diploma in Education Special Education (Autism Spectrum Disorders) D. Ed. SE (ASD)<br>
                   &emsp;&emsp;&nbsp;<strong>2.</strong>Diploma in Education Special Education (Mental Retardation) D.Ed.SE (MR)<br>
                   &emsp;&emsp;&nbsp;<strong>3.</strong>Diploma in Education Special Education (Hearing Impairment) D.Ed.SE (HI)<br>
                   &emsp;&emsp;&nbsp;<strong>4.</strong>Diploma in Vocational Rehabilitation (Mental Retardation) DVR (MR) (One Year Course)<br></p>
                   <br><h6 style="color:#7F1F21">&#9673; Admission form are available on the website (in the link forms ) and at Tamana Autism Centre (School of Hope) </h6><br>
                   <p><strong>Director(Teacher Training Cell):</strong> Ms Purnima Jain<br>
                   <strong>Sr. Coordinator D.Ed.SE (MR) :</strong> Mr. Sunil Kumar<br>
                   <strong>Course-coordinator D.Ed.SE (ASD)</strong>: Ms Archana Kumari<br>
                   <strong>Course Coordinator D.Ed.SE (HI) :</strong> Ms. Vandana<br>
                   <strong>Course Coordinator DVR(MR) </strong>: Ms.Purnina Jain </p><br>
                   <h6><a href="http://www.tamana.org/Forms/ADMISSION%20Form_ttc.pdf" style="color:#7F1F21">Click here to download the Admission/ Registration Form for Diploma in Education Special Education Programmes (ASD/MR/HI/DVR(MR))</a></h6>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color:#7F1F21;color:white">Contact:</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <p> Tamana Autism Centre (School of Hope) <br>
                   CPWD Complex <br>
                  Near Chinmaya Vidyalaya <br>
                  Vasant Vihar, New Delhi.110057<br>
                   <strong>Phone.No:-</strong>011-26143853,26153474 <br>
                <strong>Email -</strong> <a href="ttc@tamana.org">ttc@tamana.org</a><p>
              </div>
              <div class="col-md-8">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14016.540289538005!2d77.15425714451733!3d28.565706426420295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1daddc8edca9%3A0x8dfa09521b1abe83!2sSchool+of+Hope+Tamana+Association!5e0!3m2!1sen!2sin!4v1518592032983" width="750" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
            </div>
          </div>
        </div>

      </div>




@endsection
